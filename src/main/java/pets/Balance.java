package pets;

public class Balance {

    private int id;
    private double value;
    private int user_id;

    public Balance(int id, double value, int user_id) {
        this.id = id;
        this.value = value;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
