package pets;

public class Currency {

    private int id;
    private String valuta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValuta() {
        return valuta;
    }

    public void setValuta(String valuta) {
        this.valuta = valuta;
    }

    public Currency(int id, String valuta) {
        this.id = id;
        this.valuta = valuta;
    }
}
