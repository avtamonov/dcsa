package pets;

import java.util.LinkedList;
import java.util.List;

public class Item extends ShopController {

    private int id;
    private String name;
    private String type;
    private double price;

    private List<Item> petList = new LinkedList<Item>();

    public List<Item> getPetList() {
        return petList;
    }

    public void setPetList(List<Item> petList) {
        this.petList = petList;
    }

    public Item( int id, String name, String type, double price){
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public Item() { }

    @Override
    public String toString() {
      return "id: " + this.id + " name: " + this.name + " type: " + this.type + " price: " + this.price + "\n";
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
