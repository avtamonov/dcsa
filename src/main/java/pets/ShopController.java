package pets;

import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
public class ShopController {

    @GetMapping("/")
    public String sayHello(){ return "Hello"; }

    @GetMapping("/pet")
    public String getPet(){
        List<Item> itemList = new LinkedList<Item>();

        itemList.add(new Item(1, "Butch", "Pet", 90.9));
        itemList.add(new Item(2, "Lucky", "Pet", 89.67));
        itemList.add(new Item(3, "Good Boy", "Pet", 12.45));
        return itemList.toString();
    }

    @RequestMapping( value = "/pet/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getPetId( @PathVariable int id){

        List<Item> petList = new LinkedList<Item>();

        petList.add(new Item(1, "Butch", "Pet", 90.9));
        petList.add(new Item(2, "Lucky", "Pet", 89.67));
        petList.add(new Item(3, "Good Boy", "Pet", 12.45));

        return petList.get(id).toString();
    }

    @RequestMapping( value = "/balance/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getBalanceId( @PathVariable int id){

        List<Balance> balanceList = new LinkedList<Balance>();

        balanceList.add(new Balance(1, 120, 1));
        balanceList.add(new Balance(2, 720, 2));
        balanceList.add(new Balance(3, 4520, 3));

        return balanceList.get(id).toString();
    }
    //дописать
    @RequestMapping( value = "/cart/{id}/item/", method = RequestMethod.GET)
    @ResponseBody
    public String getCartId( @PathVariable int id){
        return "dog, cat";
    }

    @RequestMapping( value = "/currency/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getCurrencyId( @PathVariable int id){
        return "USD";
    }

    @RequestMapping( value = "/cart/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String postCart( @PathVariable int id){
        return "Success";
    }

    @RequestMapping( value = "/cart/pet/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public String putCart( @PathVariable int id){
        return "Put operation success";
    }

    @RequestMapping( value = "/cart/pet/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteCart( @PathVariable int id){
        return "Delete operation success";
    }

    @RequestMapping( value = "/cart/currency/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String putCartCurrency( @PathVariable int id){
        return "Put cart/currency success";
    }
}